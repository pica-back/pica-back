FROM openjdk:8-jre-alpine

RUN apk update && apk add --no-cache wget jq bash figlet

RUN mkdir -p /data/backend

COPY ./src/main/resources/ /data/backend/
COPY ./build/run.sh /run.sh

RUN chmod 644 /data/backend/* && \
    mkdir -p /data/backend/log && \
    chmod 755 /run.sh

VOLUME /tmp

CMD ["/run.sh"]

COPY ./target/backend-retrieve-products.jar /data/backend/backend-retrieve-products.jar

EXPOSE 8080