#!/bin/bash

echo "Hosts file:"
cat /etc/hosts

echo " "
figlet "Dev Essentials - Retrieve Products Backend"
echo " ";

sleep 1

DB_HOST=database-1.cf9jtxs9bqoa.us-east-2.rds.amazonaws.com
DB_NAME=inventory
DB_USER=admin
DB_PASSWORD=admin1234

echo "=> Starting service..."
java -jar /data/backend/backend-retrieve-products.jar --rds.dbinstance.host=jdbc:mysql://${DB_HOST}:3306/${DB_NAME} --rds.dbinstance.username=${DB_USER} --rds.dbinstance.password=${DB_PASSWORD}