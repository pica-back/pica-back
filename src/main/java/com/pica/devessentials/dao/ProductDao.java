package com.aws.devessentials.dao;

import com.aws.devessentials.model.Carriers;
import com.aws.devessentials.model.ResponseListProducts;

import java.sql.SQLException;
import java.util.List;

public interface ProductDao {

    ResponseListProducts getAllProducts() throws SQLException;
    Carriers getAllCarriers() throws SQLException;
}
