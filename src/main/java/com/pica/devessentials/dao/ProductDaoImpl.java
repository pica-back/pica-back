package com.aws.devessentials.dao;

import com.aws.devessentials.model.Carriers;
import com.aws.devessentials.model.DbCredentials;
import com.aws.devessentials.model.Product;
import com.aws.devessentials.model.ResponseListProducts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ProductDaoImpl implements ProductDao {

    @Autowired
    private DbCredentials dbCredentials;

    @Override
    public ResponseListProducts getAllProducts() throws SQLException {
        List<Product> products = new ArrayList<>();
        final String query = "SELECT * FROM inventory.tb_products tp JOIN inventory.tb_agreemnet ta ON productServiceDescription = ta.supplier;";

        try (
                Connection connection = ConnectionFactory.getConnection(dbCredentials);
                PreparedStatement statement = connection.prepareStatement(query);
                ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Product product = new Product();
                product.setId(resultSet.getLong("Id"));
                product.setSubCategoryId(resultSet.getString("subCategoryId"));
                product.setProductServiceDescription(resultSet.getString("productServiceDescription"));
                product.setSubCategoryDescription(resultSet.getString("subCategoryDescription"));
                    double score = ((resultSet.getInt("totalScore") * 0.5) + (resultSet.getInt("weight") * 0.5));
                product.setTotalScore(score);
                product.setNameProvider("Kallsonys");
                product.setValue(resultSet.getDouble("value"));
                products.add(product);
            }
        }
        ResponseListProducts responseServide = new ResponseListProducts();
        responseServide.setStatusCode("0");
        responseServide.setProducts(products);
        return responseServide;
    }

    @Override
    public Carriers getAllCarriers() throws SQLException {
        List<Carriers> carriers = new ArrayList<>();
        final String query = "SELECT * FROM inventory.tb_carriers WHERE state = 1 ORDER BY totalScore DESC;";

        try (
                Connection connection = ConnectionFactory.getConnection(dbCredentials);
                PreparedStatement statement = connection.prepareStatement(query);
                ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Carriers carrier = new Carriers();
                carrier.setId(resultSet.getLong("Id"));
                carrier.setCarrierName(resultSet.getString("carrierName"));
                carrier.setCarrierDescription(resultSet.getString("carrierDescription"));
                carrier.setTotalScore(resultSet.getInt("totalScore"));
                carriers.add(carrier);
            }
        }
        return carriers.get(0);
    }
}
