package com.aws.devessentials.model;

import java.io.Serializable;
import java.util.Date;

public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String subCategoryId;

    private String productServiceDescription;

    private String subCategoryDescription;
    
    private double totalScore;
    private double value;
    private String nameProvider;

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getNameProvider() {
        return nameProvider;
    }

    public void setNameProvider(String nameProvider) {
        this.nameProvider = nameProvider;
    }

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(String subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getProductServiceDescription() {
        return productServiceDescription;
    }

    public void setProductServiceDescription(String productServiceDescription) {
        this.productServiceDescription = productServiceDescription;
    }

    public String getSubCategoryDescription() {
        return subCategoryDescription;
    }

    public void setSubCategoryDescription(String subCategoryDescription) {
        this.subCategoryDescription = subCategoryDescription;
    }

    public double getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(double totalScore) {
        this.totalScore = totalScore;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", subCategoryId=" + subCategoryId + ", productServiceDescription=" + productServiceDescription + ", subCategoryDescription=" + subCategoryDescription + ", totalScore=" + totalScore + '}';
    }


}
