package com.pica.devessentials.service;

import com.aws.devessentials.model.Carriers;
import com.aws.devessentials.model.Product;
import com.aws.devessentials.model.ResponseListProducts;

import java.util.List;

public interface ProductService {

    ResponseListProducts getAllProducts();

    Carriers getAllCarriers();
}
