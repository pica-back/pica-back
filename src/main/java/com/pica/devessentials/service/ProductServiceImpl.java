package com.pica.devessentials.service;

import com.aws.devessentials.dao.ProductDao;
import com.aws.devessentials.model.Carriers;
import com.aws.devessentials.model.ResponseListProducts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDao productDao;

    public ResponseListProducts getAllProducts() {
        try {
            return this.productDao.getAllProducts();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            ResponseListProducts responseServide = new ResponseListProducts();
            responseServide.setStatusCode("1");
            return responseServide;
        }
    }

    public Carriers getAllCarriers() {
        try {
            return this.productDao.getAllCarriers();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            throw new RuntimeException("Application ran into problems while loading the products");
        }
    }
}
    
